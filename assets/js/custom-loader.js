(function(w){
    if(typeof $ != 'undefined'){
        var body = $('body:first');
        w.loader = function(){
            if (body.find('.loading-modal').length < 1) {
                body.append($('<div />').addClass('loading-modal').append($('<div />',{'class':'loader'}).text('Please Wait...')));
            }
            return{
                hide: function(){
                    body.removeClass('loading');
                    return false;
                },
                show: function(){
                    body.addClass('loading');
                    return true;
                }
            }
        }();
    }
})(window);