// core
var fs = require('fs'),
    path = require('path');

// npm
var _ = require('lodash'),
    mkdirp = require('mkdirp');

var defaults = {
    viewportSize: {width: 1024, height: 768},
    sizes: [
        {name: 'xl', width: 640, height: 480},
        {name: 'large', width: 500, height: 375},
        {name: 'medium', width: 300, height: 225},
        {name: 'small', width: 200, height: 150},
        {name: 'tiny', width: 100, height: 75},
        {name: 'l', width: 500, height: 375},
        {name: 'm', width: 300, height: 225},
        {name: 's', width: 200, height: 150},
        {name: 'xs', width: 100, height: 75}
    ],
    timeout: 20000,
    port: 5050,
    maxAge: 0,
    dataDir: path.resolve(__dirname,'../'),
    path: {
        imgPath: './imgs',
        assetsPath: './webApp',
        studentDB: './students.db',
        ipDB: './ip.db',
        cachePath: './cache.db'
    },
    serveImgs: true,
    localToRemoteUrlAccessEnabled: true,
    mysql: {
        connectionLimit: 100, //important
        host: 'localhost',
        user: 'root',
        password: '',
        database: 'address_book',
        debug: false
    },
    isDebug: true,
    unsecure: {
        id2use: '00069',
        session: '630163738'
    },
    copyright: 'jR',
    domain : 'http://unsecure.tk',
    filterUrlToLog : false,
    UrlToLog : ['get', 'search']
};

var configPath = path.resolve('./config.json'),
    config = (fs.existsSync(configPath)) ? require(configPath) : {},
    settings = defaults;
if (config != {})
    settings = _.defaults(config, defaults);

// If using OpenShift, use the dir they give write access to
var openshiftDir = process.env.OPENSHIFT_DATA_DIR;
for (var p in settings.path) {
    if (settings.path.hasOwnProperty(p)) {
        if (openshiftDir) {
            settings.path[p] = path.resolve(openshiftDir, settings.path[p]);
        } else {
            settings.path[p] = path.resolve(settings.dataDir, settings.path[p]);
        }
    }
}

// Make a home for images (if doesn't exist)
mkdirp(path.resolve(settings.path.imgPath), function (err) {
    if (err) {
        console.error('Error making img dir: ', err);
        process.exit(1);
    }
});

module.exports = settings;