/**
 * @author Muhammad Sayuthi on 6/19/2015.
 * */
var webSocket = {};
webSocket.process = function(app){
    var server = require("http").Server(app);
    var sockets = require('socket.io').listen(server);
    sockets.on('connection', function (socket) {
        socket.socketList = getAllSockets(socket);
        handleSocket(socket, sockets);
    });

    app.msmsServer = server;
};
var handleSocket = function (socket, io) {
    var client = socket.client;
    socket.broadcast.emit('connected', {id: client.id});
    socket.on('disconnect', function (){
        io.emit('disconnect', {id: client.id});
    });
    socket.on('chat message', function (msg) {
        socket.broadcast.emit('chat message', {msg: client.id + ' : ' + msg});
    });
};
function getAllSockets(socket){return socket.client.sockets}
module.exports = webSocket;