var helper = {};
(function (h) {
    h.validation = function () {
        return {
            isEmailAddress: function (str) {
                var pattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
                return pattern.test(str);  // returns a boolean
            },
            isNotEmpty: function (str) {
                var pattern = /\S+/;
                return pattern.test(str);  // returns a boolean
            },
            isNumber: function (n) {
                return /^-?\d+(\.[\d]+)?(e\d{1,3})?$/.test((n + '').replace(' ', ''));
            }
        };
    }();
    h.number = function () {
        return {
            getNumber: function (str) {
                str += '';
                var test = str.replace(',', '').match(/(-?[\d]+(\.?\d+)?)/ig) || [0];
                return parseFloat(test[0]);
            }
        };
    }();

    h.url = function () {
        return {
            getURL: function (req) {
                if (!req)return '';
                return req.protocol + '://' + req.get('host');
            }
        };
    }();
    h.array = function () {
        return {
            in: function (arr, q) {
                var length = arr.length;
                if (typeof arr == 'object' && length > 0 && (q + '').length > 0) {
                    for (var i = 0; i < length; i++) {
                        if (q.indexOf(arr[i]) > -1) return true;
                    }
                }
                return false;
            }
        }
    }();
})(helper);
module.exports = helper;