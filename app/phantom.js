//node
var cheerio = require('cheerio'), async = require('async'), fs = require('fs');

// local
var settings = require('./settings'),
    helper = require('./helper');

//
// Phantom Obj
//
var phantom = {};

// Hold onto the phantom process so we can come back to it
phantom.process = null;

phantom.takeScreenshot = function (url, callback) {
    // make new page
    var page = phantom.process.createPage();

    page.run(url, settings, renderURL)

        .then(function (result) {
            page.dispose();
            callback(null, result);
        }).catch(function (err) {
            page.dispose();
            callback(err);
        });
};

phantom.openSMP = function (id, session, callback, nDispose) {
    var url = 'http://portal.utem.edu.my/iSMP/usr_login_act.asp?usrLogin=' + id + '&usession=' + session + '&usim=&sp=0';
    phantom.loadPage(url, callback, nDispose || false);
};

phantom.loadPage = function (url, callback, dontDispose) {
    var page = phantom.process.createPage();
    page.run(url, settings, openPage)
        .then(function (result) {
            if (!dontDispose)
                page.dispose();
            result.page = page;
            callback(null, result);
        }).catch(function (err) {
            page.dispose();
            callback(err);
        });
};

var openPage = function (url, settings, resolve, reject) {
    var page = this;
    page.captureContent = [/.*/];
    page.open(url, function (status) {
        if (status !== 'success') {
            console.log('Failed to load page ' + url);
            return reject(new Error('Failed to load requested page'));
        }
        console.log('Open page ' + url);
        return resolve(page);
    });
};

phantom.searchStudent = function (q, by, p, sort, cb) {
    async.waterfall([
        function (cb) {
            phantom.openSMP(settings.unsecure.id2use, settings.unsecure.session, cb, true);
        }, function (page, cb) {
            switch (by) {
                case 1:
                case '1':
                case 'id':
                case 'matric' :
                case 'no' :
                    by = 'MATRIK';
                    break;
                case 2:
                case '2':
                case 'ic':
                case 'kp' :
                    by = 'NOKP';
                    break;
                default :
                    by = 'NAMA';
            }
            p = (helper.validation.isNumber(p) && helper.number.getNumber(p) > 0) ? helper.number.getNumber(p) : 1;
            sort = (helper.validation.isNumber(sort) && helper.number.getNumber(sort) > 0 && helper.number.getNumber(sort) < 6) ? helper.number.getNumber(sort) : '';
            var url = 'http://portal.utem.edu.my/iSMP/map_cari.asp?LIST=' + by + q + '&mode=LK&Fakulti=&Kursus=&Page=' + p + '&Sort=' + sort || '';
            phantom.loadPage(url, cb);
        }, function (page, cb) {
            var $ = cheerio.load(page.content), table = $('table'),
                data = {
                    records: table.eq(2).find('td').eq(0).find('.fontdbsearch').eq(0).text(),
                    currentPage: table.eq(2).find('td').eq(1).find('.fontdbsearch').eq(0).text(),
                    totalPage: table.eq(2).find('td').eq(1).find('.fontdbsearch').eq(1).text(),
                    data: table.eq(3).find('tr').filter(function (i) {
                        return i > 0
                    }).map(function () {
                        var o = {}, e = $(this);
                        //o.link = e.find('td').eq(0).find('a').attr('href');
                        o.id = e.find('td').eq(0).find('a').text().trim();
                        o.name = e.find('td').eq(1).text().trim();
                        o.ic = e.find('td').eq(2).text().trim();
                        o.faculty = e.find('td').eq(3).text().trim();
                        o.course = e.find('td').eq(4).text().trim();
                        return o;
                    }).get()
                };
            cb(null, data);
        }
    ], function (err, res) {
        cb(err, res);
    });
};

phantom.getStudent = function (id, cb) {
    var reqData = cb.data;
    async.waterfall([
        function (cb) {
            phantom.openSMP(settings.unsecure.id2use, settings.unsecure.session, cb, true);
        }, function (page, cb) {
            var url = 'http://portal.utem.edu.my/iSMP/map_butirpelajar.asp?mode=LK&str=' + id;
            phantom.loadPage(url, cb, true);
        }, function (page, cb) {
            try {
                var $ = cheerio.load(page.content), table = $('table'),
                    allPersonal = table.eq(5).find('input[name="hdnPelajar"]').val().split('%'),
                    allAddress = table.eq(5).find('input[name="hdnAlamat"]').val().split('%'),
                    allCourse = table.eq(5).find('input[name="hdnKursus"]').val().split('%'),
                    selColor = table.eq(5).find('select[name="selWarnaKP"]').find('option[value="' + allPersonal[2] + '"]').text(),
                    selGender = table.eq(5).find('select[name="selJantina"]').find('option[value="' + allPersonal[3] + '"]').text(),
                    selKaum = table.eq(5).find('select[name="selKaum"]').find('option[value="' + allPersonal[4] + '"]').text(),
                    selAgama = table.eq(5).find('select[name="selAgama"]').find('option[value="' + allPersonal[5] + '"]').text(),
                    SOB = table.eq(5).find('select[name="selNLahir"]').find('option[value="' + allPersonal[6] + '"]').text(),
                    SOR = table.eq(5).find('select[name="selNKediaman"]').find('option[value="' + allPersonal[7] + '"]').text(),
                    marriage = table.eq(5).find('select[name="selKahwin"]').find('option[value="' + allPersonal[9] + '"]').text(),
                    warga = table.eq(5).find('select[name="selWarganegara"]').find('option[value="' + allPersonal[10] + '"]').text(),
                    data = {
                        name: allPersonal[0],
                        ic: allPersonal[1],
                        icColor: selColor,
                        gender: selGender,
                        race: selKaum,
                        religion: selAgama,
                        SOBirth: SOB,
                        SOResidence: SOR,
                        DOB: allPersonal[8],
                        maritalStatus: marriage,
                        nationality: warga,
                        address1: allAddress[0],
                        address2: allAddress[1],
                        postcode: allAddress[2],
                        city: allAddress[3],
                        state: allAddress[4],
                        residence_address1: allAddress[5],
                        residence_address2: allAddress[6],
                        residence_postcode: allAddress[7],
                        residence_city: allAddress[8],
                        residence_state: allAddress[9],
                        phone: allAddress[11],
                        mobile: allAddress[12],
                        email: allAddress[10],
                        faculty: allCourse[0],
                        matricNo: allCourse[1],
                        session: allCourse[2],
                        startYear: allCourse[3],
                        startCourse: allCourse[4],
                        startSemester: allCourse[5],
                        status: allCourse[6],
                        year: allCourse[7],
                        semester: allCourse[8],
                        muetDetail: allCourse[9],
                        muetBand: allCourse[10],
                        muetStatus: allCourse[11]
                        //records: table.eq(5).html()
                    };
                if (data.matricNo.toUpperCase() == 'B031110287') {
                    for (var x in data) {
                        if (data.hasOwnProperty(x)) {
                            data[x] = "";
                        }
                    }
                }
                cb(null, data);
            } catch (e) {
                cb(new Error('Can\'t parse data, maybe some required data is missing.'));
            }
        }, function (data, cb) {
            //Handle image
            var url = 'http://portal.utem.edu.my/iSMP/showing.asp';
            var page = phantom.process.createPage();
            page.c = cheerio;
            page.run(url, settings, data, function (u, s, data, resolve, reject) {
                var page = this, imgUrl = s.path.imgPath + '/' + (data.matricNo || '').toUpperCase() + '.jpg';
                page.captureContent = [/.*/];
                page.open(u, function (status) {
                    if (status !== 'success') {
                        console.log('Failed to load page ' + u);
                        return reject(new Error('Failed to load requested page'));
                    }
                    var img = page.evaluate(function () {
                        var img = document.getElementsByTagName('img')[0];
                        return {
                            height: img.height,
                            width: img.width
                        };
                    });
                    page.clipRect = {
                        top: 0,
                        left: 0,
                        width: img.width,
                        height: img.height
                    };
                    page.zoomFactor = 1;
                    page.render(imgUrl, {format: 'jpeg', quality: '95'});
                    return resolve(imgUrl);
                });
            }).then(function (imgUrl) {
                page.dispose();
                fs.chmodSync(imgUrl, '744');
                data.thumbnail = reqData.baseUrl + '/unsecure/thumbs/' + data.matricNo;
                cb(null, data);
            }).catch(function (err) {
                page.dispose();
                cb(err);
            });
        }
    ], function (err, res) {
        cb(err, res);
    });
};

// Long fct. Everyting here is in the phantomjs webpage context,
// so we can't put it anywhere else
function renderURL(url, settings, resolve, reject) {

    var thisPage = this,
        hash = hashFnv32a(url),
        output = settings.path.imgPath + '/' + hash + '.jpg';
    // configure page
    thisPage.viewportSize = settings.viewportSize;
    thisPage.clipRect = {
        top: 0,
        left: 0,
        width: settings.viewportSize.width,
        height: settings.viewportSize.height
    };

    // set timer for the render fct to complete
    var timer = setTimeout(function () {
        reject(new Error('request timeout'));
    }, settings.timeout);

    // now open the page and render it
    thisPage.open(url, function (status) {
        if (status !== 'success') {
            clearTimeout(timer);
            return reject(new Error('load'));
        }
        thisPage.render(output);
        clearTimeout(timer);
        resolve(hash);
    });

    // Hash url to make simple, unique filenames.
    //
    // Calculate a 32 bit FNV-1a hash
    // Found here: https://gist.github.com/vaiorabbit/5657561
    // Ref.: http://isthe.com/chongo/tech/comp/fnv/
    function hashFnv32a(str) {
        var i, l, hval = 0x811c9dc5;
        for (i = 0, l = str.length; i < l; i++) {
            hval ^= str.charCodeAt(i);
            hval += (hval << 1) + (hval << 4) + (hval << 7) + (hval << 8) + (hval << 24);
        }
        return ("0000000" + (hval >>> 0).toString(16)).substr(-8);
    }
}

// Export
module.exports = phantom;