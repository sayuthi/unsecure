var cheerio = require('cheerio'),
    requestIp = require('request-ip'), Datastore = require('nedb'), _ = require('lodash'), fs = require('fs'), async = require('async'), phantom = require('./phantom'), settings = require('./settings'), helper = require('./helper'),
    robot = function () {
        var studentDb = new Datastore({
            filename: settings.path.studentDB,
            autoload: true
        }), ipDb = new Datastore({
            filename: settings.path.ipDB,
            autoload: true
        });
        return {
            intercept: function (req, res, next) {
                var clientIp = requestIp.getClientIp(req); // on localhost > 127.0.0.1
                if (!req.data) req.data = {};
                res.setHeader('Client-IP', clientIp);
                req.data.client = {
                    ip: clientIp
                };
                req.data.baseUrl = req.protocol + '://' + req.get('host');
                req.data.url = req.data.baseUrl + req.url;
                if (!settings.filterUrlToLog || helper.array.in(settings.UrlToLog, req.url))
                    ipDb.update({ip: clientIp, url: req.data.url}, {
                        $set: {
                            lastUpdate: _.now()
                        }, $inc: {
                            count: 1
                        }
                    }, {upsert: true}, function () {

                    });
                res.setHeader('Request-URL', req.data.url);
                if (req.data.baseUrl.indexOf("unsecure.tk") < 0 && req.url.indexOf('unsecure') < 0) {
                    return res.redirect(301, settings.domain + req.url);
                }
                next();
            },
            initRoute: function () {
                var unk1 = './assets/images/unknown.jpg', unk2 = settings.path.imgPath + '/' + 'unknown.jpg',
                    fav1 = './assets/images/favicon.ico', fav2 = settings.path.imgPath + '/' + 'favicon.ico';
                if (fs.existsSync(unk1) && !fs.existsSync(unk2)) {
                    try {
                        var inStr = fs.createReadStream(unk1);
                        var outStr = fs.createWriteStream(unk2);
                        inStr.pipe(outStr);
                    } catch (e) {
                        console.log('failed to copy : ' + unk1 + ' to ' + unk2);
                    }
                }
                if (fs.existsSync(fav1) && !fs.existsSync(fav2)) {
                    try {
                        var inStr1 = fs.createReadStream(fav1);
                        var outStr1 = fs.createWriteStream(fav2);
                        inStr1.pipe(outStr1);
                    } catch (e) {
                        console.log('failed to copy : ' + fav1 + ' to ' + fav2);
                    }
                }
            },
            searchStudent: function (req, res) {
                async.waterfall([
                    function (cb) {
                        phantom.searchStudent(req.query['q'] || '', req.query['type'] || 3, req.query['page'] || 1, req.query['sort'] || '', cb);
                    }
                ], function (err, data) {
                    if (err) return res.status(500).json({error: 'processing error', err: err});
                    res.json(data);
                });
            },
            getStudent: function (req, res) {
                async.waterfall([
                    function (cb) {
                        cb.data = req.data;
                        var id = req.params.id || '', refetch = req.query['refetch'] || false, query = {
                            matricNo: id.toUpperCase()
                        };
                        studentDb.findOne(query, function (e, data) {
                            if (!e && data && !refetch) {
                                return cb(null, data);
                            }
                            phantom.getStudent(id, cb);
                        });
                    },
                    function (data, cb) {
                        if (data['lastUpdate']) {
                            cb(null, data);
                        } else {
                            data.lastUpdate = _.now();
                            data.matricNo = data.matricNo.toUpperCase();
                            studentDb.insert(data, cb);
                        }
                    }
                ], function (err, data) {
                    if (err) return res.status(500).json({error: 'processing error', err: err});
                    res.json(data);
                });
            },
            getThumbs: function (req, res) {
                var uid = (req.params.id || '').toUpperCase(), url = settings.path.imgPath + '/' + uid + '.jpg',
                    opt = ''
                    ;
                for (var q in req.query) {
                    if (req.query.hasOwnProperty(q)) {
                        opt += (q + '=' + req.query(q) + '&');
                    }
                }

                var options = {
                    dotfiles: 'deny',
                    headers: {
                        'x-timestamp': Date.now(),
                        'x-serve-by': settings.copyright,
                        'message2seeker': 'Just shut the fuck up and learn more. There are nothing perfect!',
                        'x-sent': true
                    }
                };
                try {
                    fs.chmodSync(url, '744');
                } catch (e) {
                }
                fs.exists(url, function (exists) {
                    if (exists) {
                        res.sendFile(url, options);
                    } else {
                        async.waterfall([
                            function (cb) {
                                phantom.getStudent(req.params.id || '', cb);
                            }
                        ], function (err, data) {
                            fs.exists(url, function (exists) {
                                if (exists) {
                                    return res.sendFile(url, options);
                                } else {
                                    return res.status(500).json({error: 'Cant process this data for this moment.', err: {message: '=) kah kah'}});
                                }
                            });
                        });
                    }
                });
            }, processSMP: function (page) {
                page.switchToFrame('menu');
                var $ = cheerio.load(page.frameContent), json = {}, tblInfo = $('table').first();
                json.name = tblInfo.find('td').eq(1).text();
                json.title = tblInfo.find('td').eq(2).text().replace(/(^\[?\s+)|(\s+]?$)/g, '');
                return json;
            }, requestGetIP: function (req, res) {
                var ip = req.params.ip || req.data.client['ip'];
                robot.getIPDetails(ip, function (e, data) {
                    if (!e) {
                        return res.json(data);
                    } else {
                        return res.status(500).json({error: 'Cant process this data for this moment.', err: {message: '=) kah kah'}});
                    }
                })
            }, getIPDetails: function (ip, callback) {
                async.waterfall([function (cb) {
                    if (ip) {
                        phantom.loadPage('http://www.iptrace.in/' + ip, cb);
                    }
                }, function (page, cb) {
                    var $ = cheerio.load(page.content || ''), table1 = $('.DivTables').eq(0), result = {};
                    table1.find('.DivTableColumn').filter(function (i) {
                        return i > 1 && (i % 2 == 1);
                    }).map(function () {
                        var o = {}, e = $(this), detail = e.prev('.DivTableColumn'), keys = ['ip', 'owner', 'location', 'country'];
                        o.text = detail.text().trim();
                        o.value = e.text().trim();
                        keys.forEach(function (key) {
                            if (o.text.toLowerCase().indexOf(key.toLowerCase()) != -1) {
                                result[key] = o;
                                return o;
                            }
                        });
                        return o;
                    });
                    var sc = $('script').filter(function (i, e) {
                        return !$(e).is('[src]');
                    }).text().match(/google.maps.LatLng\(([^)]+)\)/);
                    result.latitude = 0;
                    result.longitude = 0;
                    if (sc && sc.length > 1 && sc[1].split(', ').length == 2) {
                        result.latitude = helper.number.getNumber(sc[1].split(', ')[0]);
                        result.longitude = helper.number.getNumber(sc[1].split(', ')[1]);
                    }
                    cb(null, result);
                }], function (e, data) {
                    callback(e, data);
                });
            }
        }
    }();

module.exports = robot;