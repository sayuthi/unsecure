// core
var fs = require('fs'),
    path = require('path');

// npm
var cheerio = require('cheerio');
var _ = require('lodash'),
    gm = require('gm'),
    cors = require('cors'),
    async = require('async'),
    validUrl = require('valid-url'),
    Datastore = require('nedb'),
    useragent = require('express-useragent');

// local
var phantom = require('./phantom'),
    settings = require('./settings'),
    convert = require('./convert'),
    helper = require('./helper'),
    robot = require('./robot');

var db = new Datastore({
    filename: settings.path.cachePath,
    autoload: true
});