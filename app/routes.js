// core
var fs = require('fs'),
    path = require('path'),
    favicon = require('serve-favicon');
// npm
var cheerio = require('cheerio');
var _ = require('lodash'),
    gm = require('gm'),
    cors = require('cors'),
    async = require('async'),
    qt = require('quickthumb'),
    validUrl = require('valid-url'),
    Datastore = require('nedb'),
    useragent = require('express-useragent');

// local
var phantom = require('./phantom'),
    settings = require('./settings'),
    convert = require('./convert'),
    helper = require('./helper'),
    robot = require('./robot');


// Configure
var corsOptions = {
    origin: function (origin, callback) {
        // CORS ok for all
        if (_.isBoolean(settings.cors) && settings.cors) {
            return callback(null, true);
        }
        // CORS by whitelist only
        if (settings.cors && !_.contains(settings.cors, origin)) {
            return callback(null, true);
        }
        // no CORS for you
        callback(null, false);
    }
};

var db = new Datastore({
    filename: settings.path.cachePath,
    autoload: true
});

var im = gm.subClass({imageMagick: true});

var MS_PER_DAY = 1000 * 60 * 60 * 24,
    MAX_AGE = MS_PER_DAY * settings.maxAge;


// Middleware
function checkKey(req, res, next) {
    // ghetto obscurity security!
    //
    // Without implementing something more complex, you have to choose:
    // 1- Authorizing requests with a secret API key (secure)
    // 2- Making service available to client side js (no security)
    //
    // If you choose 2, we can still try to check API keys for requests
    // originating from places other than a browser. I know, I know,
    // obviously this can be spoofed, but adding an annoying hoop might
    // prevent the less motivated from trying to use the service unauthorized.
    //
    // Here we check if cors is enabled (a sign that we want client side usage)
    // and if the request looks like if came from a browser we skip the key
    // check.
    if (settings.cors && looksLikeBrowser(req)) return next();

    if (settings.key && req.param('key') !== settings.key) {
        return res.status(403).json({error: 'access denied'});
    }
    next();
}

// No way to be sure if its a browser, but we can be annoying
function looksLikeBrowser(req) {
    return !!req.get('origin') &&
        (req.useragent.isDesktop || req.useragent.isMobile);
}

function notFoundHandler(req, res, next) {
    res.status(404).json({
        code: 404,
        error: '404 Not Found',
        message: 'Page / Resource not found'
    });
}


// Route logic
function getScreenshot(req, res) {
    var url = req.query['url'],
        size = req.query['size'] || 's',
        urlErr = (!url || !validUrl.isWebUri(url)),
        sizeErr = (!size || !_.find(settings.sizes, {name: size}));
    // console.log(req);
    if (urlErr)  return res.status(400).json({error: 'bad url'});
    if (sizeErr) return res.status(400).json({
        error: 'bad size',
        acceptSize: settings.sizes
    });

    var query = {
        url: url,
        time: {
            $gte: _.now() - MAX_AGE
        }
    };

    // check cache first for anything fresh enough
    db.findOne(query, function (err, doc) {
        if (err) return res.status(500).json({error: 'db error'});
        if (doc) return res.json({img: getURL(doc.img, size, req)});

        // if not in cache, need to process screenshot
        processUrl(url, function (err, doc) {
            if (err) return res.status(500).json({error: 'processing error', err: err});
            res.json({img: getURL(doc.img, size, req)});
        });
    });
}

function processUrl(url, onComplete) {
    var imgBase;

    async.waterfall([
        function (cb) {
            phantom.takeScreenshot(url, cb);
        },
        function (base, cb) {
            imgBase = base;
            resizeImages(imgBase, cb);
        },
        function (cb) {
            // Delete viewport render after resizing
            fs.unlink(getPath(imgBase), cb);
        },
        function (cb) {
            save(url, imgBase, cb);
        }
    ], onComplete);
}

function resizeImages(imgBase, onComplete) {
    var fullsizeImg = getPath(imgBase);

    async.each(settings.sizes, function (size, callback) {
        im(fullsizeImg)
            .resize(size.width, size.height, '!')
            .write(getPath(imgBase, size.name), callback);
    }, onComplete);
}

function save(url, imgBase, onComplete) {
    db.insert({
        url: url,
        img: imgBase,
        time: _.now()
    }, onComplete);
}

// Format img names: %hash%-%size%.jpg
function getName(base, size) {
    if (!size) return base + '.jpg';
    return base + '-' + size + '.jpg';
}

// Get absolute path to image
function getPath(base, size) {
    return path.resolve(settings.path.imgPath, getName(base, size));
}

// If node is serving the images, return the location
// else just return the image name
function getURL(base, size, req) {
    if (!settings.serveImgs) return getName(base, size);
    return req.protocol + '://' + req.get('host') + '/imgs/' +
        getName(base, size);
}

//
// Export express routes/middleware
//
module.exports = function (app, express) {
    var webSocket = require('./webSocket.js');
    robot.initRoute();
    app.disable('x-powered-by');
    app.use(cors(corsOptions));
    app.use(robot.intercept);
    app.use(useragent.express());
    // Routes
    app.get('/', function (req, res) {
        //res.json(req.data);
        res.render('index');
    });
    // Optionally serve imgs
    if (settings.serveImgs) {
        app.use('/imgs', express.static(settings.path.imgPath));
        app.use('/images', express.static(settings.path.imgPath));
    }
    app.use('/js', express.static(settings.path.assetsPath + '/js'));
    app.use('/assets', express.static('./assets'));
    app.use('/chat(room|box)?', express.static('./webApp/index.html'));
    app.get('/s(cr(een)?-?s)?hot', checkKey, getScreenshot);
    app.get('/convert/[a-z]+[a-z0-9]*', function (req, res) {
        var _url = (req.url || '');
        res.json(convert.getResult(_url));
    });
    app.get('/robot', function (req, res) {
        async.waterfall([
            function (cb) {
                phantom.openSMP(settings.unsecure.id2use, settings.unsecure.session, cb);
            }
        ], function (err, ok) {
            if (err) return res.status(500).json({error: 'processing error', err: err});
            res.json(robot.processSMP(ok));
        });
    });
    app.get('/unsecure', function (req, res) {
        res.render('unsecure', {title: 'STFU!'});
    });
    app.get('/test', function (req, res) {
        async.waterfall([function (cb) {

        }], function () {
            db.findOne();
        });
    });
    app.use(favicon(settings.path.imgPath + '/favicon.ico'));
    app.get('/unsecure/search', robot.searchStudent);
    app.get('/unsecure/get/:id', robot.getStudent);
    app.use('/thumbs', qt.static(settings.path.imgPath, {type: 'crop', cacheDir: settings.path.imgPath + '/.cache/', quality: 0.95}));
    app.get('/unsecure/thumbs/:id', robot.getThumbs);
    app.get('/ip/:ip', robot.requestGetIP);
    app.get('/dummy', function (req, res) {
        return res.json({

            records: 3,
            currentPage: 1,
            totalPage: 1,
            data: [{
                id: 'b031123213',
                name: req.query['q'] + ' 1',
                ic: '896868767868',
                faculty: 'gfsdf',
                course: 'sdfgsdgs'
            },{
                id: 'bj213123123',
                name: req.query['q'] + ' 2',
                ic: '7868787686',
                faculty: 'ashgd',
                course: 'zxjcgk'
            },{
                id: 'c25742459',
                name: req.query['q'] + ' 3',
                ic: '875765864',
                faculty: 'hkjh',
                course: 'jhjhk'
            }
            ]
        });
    });
    webSocket.process(app);
    // 404
    app.use(notFoundHandler);
};