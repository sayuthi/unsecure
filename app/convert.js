var convert = {}, helper = require('./helper');
(function(_){
    var getSectionArray = function(url){
        return (url || '').replace('/convert/', '').split('/').filter(function (a) {
            return a != '' && a != undefined;
        });
    },getParam = function(url){
        var _arr = getSectionArray(url);
        return {
            section: (_arr.length > 0) ? _arr[0] : '',
            params: _arr.splice(1)
        };
    },process = function(section,params){
        var _res;
        if(/^min(ute(s)?)?(2|to|-)sec(ond(s)?)?$/.test(section) && params.length > 0){
            _res = {
                seconds : helper.number.getNumber(params[0]) * 60
            }
        }else if(/^sec(ond(s)?)?(2|to|-)min(ute(s)?)?$/.test(section) && params.length > 0){
            _res = {
                minutesOnly : helper.number.getNumber(params[0]) / 60,
                minutes : (helper.number.getNumber(params[0]) - helper.number.getNumber(params[0]) % 60) / 60,
                seconds : helper.number.getNumber(params[0]) % 60
            }
        }else if(/^min(ute(s)?)?(2|to|-)hour(s)??$/.test(section) && params.length > 0){
            _res = {
                hoursOnly : helper.number.getNumber(params[0]) / 60,
                hours : (helper.number.getNumber(params[0]) - helper.number.getNumber(params[0]) % 60) / 60,
                minutes : helper.number.getNumber(params[0]) % 60
            }
        }
        return _res;
    };
    _.getResult = function(url){
        var _param = getParam(url);
        return {
            section: _param.section,
            params: _param.params,
            results : process(_param.section,_param.params)
        }
    }
})(convert);
module.exports = convert;