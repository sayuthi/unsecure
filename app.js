// npm
var phridge = require('phridge'),
    express = require('express');

// local
var phantom = require('./app/phantom'),
    settings = require('./app/settings');

// OpenShift ready port/ip
var port = process.env.OPENSHIFT_NODEJS_PORT || settings.port,
    ip = process.env.OPENSHIFT_NODEJS_IP;

// set up express, routes, middleware
var app = express();
app.port = port;
app.set('view engine', 'ejs');
require('./app/routes.js')(app, express);

// spawn some phantom!
phridge.spawn({
    // passing CLI-style options does also work
    //"--remote-debugger-port": 8888
}).then(function (process) {
    phantom.process = process;
    if (app.msmsServer)
        app.msmsServer.listen(port, ip);
    else
        app.listen(port, ip);
});